package Base;

import api.BookApartmentAPI;
import api.GetHotelsAndLocationsAPI;

public class APIBaseSteps {

    public BookApartmentAPI bookApartmentAPI = new BookApartmentAPI();
    public GetHotelsAndLocationsAPI getHotelsAndLocationsAPI = new GetHotelsAndLocationsAPI();
}
