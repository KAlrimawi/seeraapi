### **Seera Automation API Test**

#### Getting Started
    
Setup your machine:

* Install JDK 1.8

* Install IntelliJ (Community edition is fine)

* Install Cucumber Plugin To IntelliJ

* Install maven

#### Running tests through maven:
* To run BookApartment.feature run this command:
mvn test -Dcucumber.options="--tags @book_apartment"

* To run GetHotelsAndLocations.feature run this command:
mvn test -Dcucumber.options="--tags @get_hotels_and_locations"

* To run both scenarios run this command:
mvn install -Dcucumber.options="--tags @all"

### Reports

* Open courgette-report folder from target directory once the execution finished.
* Open `index.html` using any web browser
* Report will generate for each TestFlow.

#### Running tests from intellij:
* Right Click On The Feature You Want To Run And Select Run Feature - here you can check the results through console (Passed/Failed)